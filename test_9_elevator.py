"""
Company: Veevart
Author: Simon Zuluaga

Solution for elevator simulation question for Veevart technical interview
"""

msg = {
        "status": "Elevador en piso",
        "stop": "Elevador se detiene",
        "input": "Piso ingresado",
        "up": "Elevador subiendo",
        "down": "Elevador descendiendo",
        }

def elevator_simulation(start_floor, direction, floors, input_floors):
    """
    Simulate elevator movement from an initial floor and direction to meet a
    list of floor petitions with a list of inputed floor petitions after landing
    on certain floors.

    Args:
        start_floor (int): Initial floor for the elevator
            direction.
        direction (str): Initial elevator direction.
        floors (list[int]): List of floors petitions to meet by the elevator.
        input_floors (dict[int, int]): Dictionary of inputed floors at certain
            floors.
    """

    is_first_iter = True
    current_floor = start_floor

    # Simulate until all petitions are meet
    while len(floors) > 0:
        print(f"{msg["status"]} {current_floor}")

        # Acosional first direction log
        if is_first_iter:
            print(f"{msg[direction]}")
            is_first_iter = False

        # When a petition is encountered in the movement
        if current_floor in floors:
            floors.remove(current_floor)

            # Log state after meeting the petition
            if len(floors) > 0:
                print(f"{msg["stop"]}\t->{floors}")
            else:
                print(f"{msg["stop"]}")

            # Add input floor
            if current_floor in input_floors:
                new_floor = input_floors.pop(current_floor)
                if not new_floor in floors:
                    floors.append(new_floor)
                print(f"{msg["input"]} {new_floor}\t->{floors}")

            # Check direction
            if len(floors) > 0:
                if current_floor < floors[0]:
                    direction = "up"
                elif current_floor > floors[0]:
                    direction = "down"
                print(f"{msg[direction]}")

        # Move elevator
        if "up" == direction:
            current_floor += 1
        else:
            current_floor -= 1

def elevator_game(start_floor, floors):
    """
    Simulate elevator movement from an initial floor to meet a
    list of floor petitions while expecting user requests.
    User requests must be only the floor number where the elevator is requested.

    Args:
        start_floor (int): Initial floor for the elevator
            direction.
        floors (list[int]): List of floors petitions to meet by the elevator.
    """

    # Initialize values
    is_first_iter = True
    current_floor = start_floor
    if current_floor < floors[0]:
        direction = "up"
    elif current_floor > floors[0]:
        direction = "down"

    # Simulate until all petitions are meet
    while len(floors) > 0:
        print(f"{msg["status"]} {current_floor}")

        # Acosional first direction log
        if is_first_iter:
            print(f"{msg[direction]}")
            is_first_iter = False

        # When a petition is encountered in the movement
        if current_floor in floors:
            floors.remove(current_floor)

            # Log state after meeting the petition
            if len(floors) > 0:
                print(f"{msg["stop"]}\t->{floors}")
            else:
                print(f"{msg["stop"]}")

            # Check direction
            if len(floors) > 0:
                if current_floor < floors[0]:
                    direction = "up"
                elif current_floor > floors[0]:
                    direction = "down"
                print(f"{msg[direction]}")

        # Recieve user input
        request = input("Elevator request floor (enter if none): ").strip()
        if request != "":

            # End game
            if request == "q":
                return

            new_floor = int(request)
            if not new_floor in floors:
                floors.append(new_floor)
            print(f"{msg["input"]} {new_floor}\t->{floors}")

        # Move elevator
        if "up" == direction:
            current_floor += 1
        else:
            current_floor -= 1

if __name__ == '__main__':
    t_floors = [5, 29, 13, 10]
    t_input_floors = { 5: 2, 29: 10, 13: 1, 10: 1 }
    elevator_simulation(4, "up", t_floors, t_input_floors)
    t_floors = [5, 29, 13, 10]
    # elevator_game(4, t_floors)

