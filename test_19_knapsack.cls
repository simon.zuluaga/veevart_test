public class Knapsack {
    public static List<List<Integer>> solve(List<List<Integer>> elements, Integer m) {
        // Initialize tabularization
        Integer n = elements.size();
        List<List<Integer>> track = new List<List<Integer>>();
        for (Integer i = 0; i < n + 1; i++) {
            List<Integer> tmp = new List<Integer>();
            for (Integer j = 0; j < m + 1; j++) {
                tmp.add(0);
            }
            track.add(tmp);
        }
        
        // Optimize
        for (Integer i = 1; i < n + 1; i++) {
            for (Integer w = 1; w < m + 1; w++) {
                if (elements[i - 1][0] > w) {
                    track[i][w] = track[i - 1][w];
                } else {
                    Integer exclude = track[i - 1][w];
                    Integer include = track[i - 1][w - elements[i - 1][0]] + elements[i - 1][1];
                    if (exclude > include) {
                        track[i][w] = exclude;
                    } else {
                        track[i][w] = include;
                    }
                }
            }
        }
        
        List<Integer> maxValCont = new List<Integer>();
        maxValCont.add(track[n][m]);

        // Find optimization elements
        Integer i = n;
        Integer w = m;
        List<List<Integer>> picked = new List<List<Integer>>();
        while (w > 0) {
            if (track[i][w] != track[i - 1][w]) {
                if (picked.size() > 0) {
                    picked.add(0, elements[i - 1]);
                } else {
                    picked.add(elements[i - 1]);
                }
                w -= elements[i - 1][0];
            }
            i -= 1;
        }

        List<List<Integer>> answer = new List<List<Integer>>(picked);
        answer.add(maxValCont);
        
        System.debug(answer);
        return answer;
    }
}

/* To run in apex */
/*
List<Integer> e0 = new List<Integer>();
e0.add(2);
e0.add(3);
List<Integer> e1 = new List<Integer>();
e1.add(3);
e1.add(4);
List<Integer> e2 = new List<Integer>();
e2.add(4);
e2.add(5);
List<Integer> e3 = new List<Integer>();
e3.add(5);
e3.add(6);

List<List<Integer>> elements = new List<List<Integer>>();
elements.add(e0);
elements.add(e1);
elements.add(e2);
elements.add(e3);

Knapsack.solve(elements, 10);
*/
