# Veevart technical interview for Simon Zuluaga

## How to execute solutions

To execute the provided test case for any solution, run each file as the
entrypoint.

```sh
python test_19_knapsack.py
python test_9_elevator.py
```

### Test 19 (knapsack)

To run the knapsack solution with custom data, run the python interpreter with
the file test_19_knapsack.py:

```sh
python -i test_19_knapsack.py
```

Then define your data and call the *knapsack* function with it:
```py
>>> # define your variables: elements, capacity
>>> knapsack(elements, capacity)
```

**Apex**: To run the apex version look at the file 'test_19_knapsack.cls' and
execute the 'Knapsack.solve' function. The last tuple in the answer is the
maximum value. Test data is provided in the file comments

### Test 9 (elevator)

To run the elevator simulation with custom data, run the python interpreter with
the file test_9_elevator.py:

```sh
python -i test_9_elevator.py
```

To call the basic elevator simulation, call the *elevator_simulation* function
with the defined data:

```py
>>> # define your variables: start_floor, direction, floors, input_floors
>>> elevator_simulation(start_floor, direction, floors, input_floors)
```

To call the elevator game (user input), call the *elevator_game* function with
the defined data:

```py
>>> # define your variables: start_floor, floors
>>> elevator_game(start_floor, floors)
```

