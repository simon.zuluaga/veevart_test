"""
Company: Veevart
Author: Simon Zuluaga

Solution for knapsack question for Veevart technical interview
"""

def knapsack(elements, m):
    """
    Find optimal subset of elements maximazing the total value without exceding
    a given capacity.

    Args:
        elements (list[(int, int)]): List of elements as tuples of weight (w_i)
        and value (v_i).
        m (int): Capacity constraint.

    Returns:
        picked (list[(int, int)]): List of picked elements for optimal solution.
        picked_value (int): Maximum posible value for 'elements' and 'm'.
        leftout (list[(int, int)]): List of elements absent in optimal solution.
        leftout_value (int): Total value of 'leftout' elements.
    """
    n = len(elements)

    # Init tabularization
    track = [[0] * (m + 1) for _ in range(n + 1)]

    # Optimize
    for i in range(1, n + 1):
        for w in range(1, m + 1):
            if elements[i - 1][0] > w:
                track[i][w] = track[i - 1][w]
            else:
                exclude = track[i - 1][w]
                include = track[i - 1][w - elements[i - 1][0]] + elements[i - 1][1]
                track[i][w] = max(exclude, include)

    # Find optimization elements
    i, w = n, m
    includes = []
    while w > 0:
        if track[i][w] != track[i - 1][w]:
            includes.append(i - 1)
            w -= elements[i -1][0]
        i -= 1

    # Format solution
    picked, leftout = [], []
    picked_value, leftout_value = track[n][m], 0
    for i in range(n):
        if i in includes:
            picked.append(elements[i])
        else:
            leftout.append(elements[i])
            leftout_value += elements[i][1]

    return {
            "picked": picked,
            "picked_value": picked_value,
            "leftout": leftout,
            "leftout_value": leftout_value
            }

if __name__ == '__main__':
    test_elements = [(2, 3), (3, 4), (4, 5), (5, 6)]
    ans = knapsack(test_elements, 10)

    for k, v in ans.items():
        print(f"{k}: {v}")

